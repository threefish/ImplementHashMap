### 2019年10月12日 深入了解HashMap实现原理
- 
### 开始实践
- 1、手写一套HashMap
    - 在未加入自动扩容前,放入100000个数据需要 （7000-8000 ms）（多次运行），有点吓人，和JDK提供的HashMap（40-50ms）差距是160倍左右
    - 加入自动扩容后,放入100000个数据需要 50-70 ms（多次运行）已经非常接近JDK提供的HashMap了
### 测试数据

#### MyHashMap 单线程下连续运行10次
```text
单线程第[1]次运行 耗时 255 ms
单线程第[2]次运行 耗时 192 ms
单线程第[3]次运行 耗时 134 ms
单线程第[4]次运行 耗时 235 ms
单线程第[5]次运行 耗时 530 ms
单线程第[6]次运行 耗时 96 ms
单线程第[7]次运行 耗时 140 ms
单线程第[8]次运行 耗时 216 ms
单线程第[9]次运行 耗时 97 ms
单线程第[10]次运行 耗时 96 ms
```
#### JDK HashMap 单线程下连续运行10次
```text
单线程第[1]次运行 耗时 270 ms
单线程第[2]次运行 耗时 179 ms
单线程第[3]次运行 耗时 151 ms
单线程第[4]次运行 耗时 137 ms
单线程第[5]次运行 耗时 501 ms
单线程第[6]次运行 耗时 108 ms
单线程第[7]次运行 耗时 144 ms
单线程第[8]次运行 耗时 250 ms
单线程第[9]次运行 耗时 109 ms
单线程第[10]次运行 耗时 109 ms
```
----
#### MyHashMap 多线程下
```text
MyHashMap 线程[0]   耗时 4209 ms
MyHashMap 线程[5]   耗时 4216 ms
MyHashMap 线程[3]   耗时 4218 ms
MyHashMap 线程[7]   耗时 4223 ms
MyHashMap 线程[8]   耗时 4225 ms
MyHashMap 线程[2]   耗时 4415 ms
MyHashMap 线程[9]   耗时 4415 ms
MyHashMap 线程[1]   耗时 4415 ms
MyHashMap 线程[6]   耗时 4422 ms
MyHashMap 线程[4]   耗时 4427 ms
```
#### JDK1.7 HashMap 多线程下
```text
JDK HashMap线程[9]   耗时 4386 ms
JDK HashMap线程[2]   耗时 4400 ms
JDK HashMap线程[1]   耗时 4402 ms
JDK HashMap线程[4]   耗时 4603 ms
JDK HashMap线程[6]   耗时 4605 ms
JDK HashMap线程[0]   耗时 4607 ms
JDK HashMap线程[5]   耗时 4608 ms
JDK HashMap线程[8]   耗时 4610 ms
JDK HashMap线程[3]   耗时 4613 ms
JDK HashMap线程[7]   耗时 4615 ms
```
#### JDK1.8 HashMap 多线程下
```text
JDK HashMap线程[1]   耗时 3039 ms
JDK HashMap线程[6]   耗时 3058 ms
JDK HashMap线程[2]   耗时 3275 ms
JDK HashMap线程[4]   耗时 3276 ms
JDK HashMap线程[8]   耗时 3275 ms
JDK HashMap线程[5]   耗时 3278 ms
JDK HashMap线程[7]   耗时 3281 ms
JDK HashMap线程[0]   耗时 3283 ms
JDK HashMap线程[9]   耗时 3284 ms
JDK HashMap线程[3]   耗时 3287 ms
```


### 归纳总结
- 
- 待整理...

    