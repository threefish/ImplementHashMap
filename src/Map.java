/**
 * @author huchuc@vip.qq.com
 * @date: 2019/10/12
 */
public interface Map<K, V> {

    int size();

    V get(K key);

    V put(K key, V value);


    interface Entry<K, V> {

        K getKey();

        V getValue();

    }
}
